import React from 'react';
import Route from './src/Route';
import firebase from '@react-native-firebase/app';

let firebaseConfig = {
  apiKey: 'AIzaSyDQsF7sfeO_Lhx5OU5xIphGAM2Y1hPaNUc',
  authDomain: 'cookingapp-3aa19.firebaseapp.com',
  projectId: 'cookingapp-3aa19',
  storageBucket: 'cookingapp-3aa19.appspot.com',
  messagingSenderId: '435246797331',
  appId: '1:435246797331:web:309607204327ada07ca5e6',
  measurementId: 'G-N0C48PCKSX',
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const App = () => {
  return <Route />;
};

export default App;
