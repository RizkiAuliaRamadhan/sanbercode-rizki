import React from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';
import {Icon, Avatar, Text} from 'react-native-elements';

const Tentang = ({route}) => {
  // console.log(route.params);
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.container}>
        <Avatar
          rounded
          source={require('../../images/profil.png')}
          size={150}
        />
        <View style={{height: 10}} />
        <Text h4>Rizki Aulia Ramadhan</Text>
        <Text h4Style style={{fontSize: 16}}>
          Mahasiswa | STIMIK Tunas Bangsa
        </Text>
        <View style={{height: 10}} />
        <Text h4>Tentang Aplikasi</Text>
        <View style={styles.line} />
        <Text h4Style style={styles.about}>
          Aplikasi ini dibuat untuk memenuhi project akhir SanberCode Mobile App
          Development (Batch 25). Dalam aplikasi ini saya menggunakan Public API
          yang memiliki base URL (https://masak-apa.tomorisakura.vercel.app) dan
          untuk Authentikasi menggunakan Firebase. Untuk dokumentasi API bisa
          kunjungi github
          (https://github.com/tomorisakura/unofficial-masakapahariini-api)
        </Text>
        <View style={{height: 10}} />
        <Text h4>Akun</Text>
        <View style={styles.line2} />
        <View
          style={{
            flexDirection: 'row',
            marginTop: 10,
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Icon
            name="github"
            // color="#fff"
            type="fontisto"
            style={{fontSize: 30}}
          />
          <Text h4Style style={{fontSize: 16}}>
            {' '}
            https://github.com/RizkiAuliaRamadhan
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 10,
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Icon
            name="gitlab"
            // color="#fff"
            type="fontisto"
            style={{fontSize: 30}}
          />
          <Text h4Style style={{fontSize: 16}}>
            {' '}
            https://gitlab.com/RizkiAuliaRamadhan
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 10,
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Icon
            name="linkedin"
            // color="#fff"
            type="fontisto"
            style={{fontSize: 30}}
          />
          <Text h4Style style={{fontSize: 16}}>
            {' '}
            Rizki Aulia R
          </Text>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    marginVertical: 10,
    paddingHorizontal: 10,
    flex: 1,
    alignItems: 'center',
  },
  line: {
    borderWidth: 1,
    width: 180,
  },
  line2: {
    borderWidth: 1,
    width: 60,
  },
  about: {
    fontSize: 16,
    marginTop: 10,
    textAlign: 'justify',
  },
});

export default Tentang;
