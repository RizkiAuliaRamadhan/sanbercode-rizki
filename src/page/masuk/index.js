import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  Alert,
} from 'react-native';
import auth from '@react-native-firebase/auth';
import {Input, Icon} from 'react-native-elements';

const Masuk = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [security, setSecurity] = useState(true);

  const onLoginPress = () => {
    if (email == '' || password == '') {
      Alert.alert('Gagal', 'Form harus diisi');
    } else {
      auth()
        .signInWithEmailAndPassword(email, password)
        .then(res => {
          console.log(res.user.email);
          Alert.alert('Berhasil', 'Selamat Anda berhasil login');
          navigation.navigate('Bottom', {
            email: res.user.email,
          });
        })
        .catch(err => {
          Alert.alert('Gagal', err.message);
        });
    }
  };
  return (
    <ImageBackground
      source={require('../../images/1.jpg')}
      style={{
        width: '100%',
        height: '100%',
        flex: 1,
        justifyContent: 'flex-end',
      }}>
      <View style={styles.card}>
        <Input
          placeholder="email@address.com"
          label="Email"
          value={email}
          onChangeText={email => setEmail(email)}
          leftIcon={<Icon name="mail" type="ionicon" color="#888" />}
        />
        <Input
          placeholder="Password"
          label="Password"
          value={password}
          onChangeText={password => setPassword(password)}
          leftIcon={<Icon name="lock-closed" type="ionicon" color="#888" />}
          rightIcon={
            <Icon
              name={security ? 'eye-off' : 'eye'}
              type="ionicon"
              color="#888"
              onPress={() => {
                setSecurity(prev => !prev);
              }}
            />
          }
          secureTextEntry={security}
        />
        <TouchableOpacity style={styles.masuk} onPress={() => onLoginPress()}>
          <Text style={styles.textTombol}>Masuk</Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  card: {
    height: '70%',
    backgroundColor: '#F9F9F9',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderWidth: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: '10%',
    // paddingVertical: '20%',
  },
  masuk: {
    width: '100%',
    backgroundColor: '#007bff',
    padding: 10,
    borderRadius: 10,
    marginTop: 20,
  },
  textTombol: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 16,
  },
  label: {
    width: '75%',
    opacity: 0.5,
    marginBottom: 5,
    marginTop: 5,
  },
  input: {
    borderWidth: 1,
    borderColor: '#17a2b8',
    width: '75%',
    paddingVertical: -10,
    paddingHorizontal: 10,
    borderRadius: 5,
  },
});

export default Masuk;
