import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
} from 'react-native';
import {SearchBar} from 'react-native-elements';
import Axios from 'axios';
import {Icon, Image} from 'react-native-elements';
import {useNavigation} from '@react-navigation/core';

const Search = () => {
  const [search, setSearch] = useState('');
  const [items, setItems] = useState([]);

  const navigation = useNavigation();

  useEffect(() => {
    Axios.get(
      `https://masak-apa.tomorisakura.vercel.app/api/search/?q=${search}`,
    )
      .then(res => {
        if (search == '') {
          console.log('data kosong');
        } else {
          setItems(res.data.results);
        }
      })
      .catch(err => console.log(err));
  });

  return (
    <ScrollView showsVerticalScrollIndicator={false} style={{padding: 10}}>
      <TouchableOpacity
        style={{flexDirection: 'row', alignItems: 'center', marginBottom: 10}}
        onPress={() => {
          navigation.goBack();
        }}>
        <Icon
          name="arrow-back-circle-sharp"
          type="ionicon"
          color="#007bff"
          size={30}
        />
        <View style={{width: 10}} />
      </TouchableOpacity>
      <SearchBar
        placeholder="Cari Resep"
        value={search}
        onChangeText={text => {
          setSearch(text);
        }}
        lightTheme={true}
        // showLoading={true}
      />
      <View>
        {items.map((value, index, array) => {
          return (
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('Detail', {
                  key: array[index].key,
                });
              }}>
              <Image
                source={{uri: array[index].thumb}}
                style={styles.image}
                resizeMode="cover"
                transition
                transitionDuration="1000"
              />
              <View style={styles.card}>
                <Text style={styles.textImageCard}>{array[index].title}</Text>
                <View style={styles.itemCardDetail}>
                  <Icon
                    name="alarm"
                    color="#fff"
                    style={styles.icon}
                    type="ionicon"
                  />
                  <Text style={styles.itemTextCard}>{array[index].times}</Text>
                  <View style={{width: 10}} />
                  <Icon
                    name="cafe"
                    color="#fff"
                    style={styles.icon}
                    type="ionicon"
                  />
                  <Text style={styles.itemTextCard}>
                    {array[index].serving}
                  </Text>
                  <View style={{width: 10}} />
                  <Icon
                    name="pulse"
                    color="#fff"
                    style={styles.icon}
                    type="ionicon"
                  />
                  <Text style={styles.itemTextCard}>
                    {array[index].difficulty}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          );
        })}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  image: {
    width: '100%',
    height: 200,
    marginVertical: 5,
    borderRadius: 5,
  },
  card: {position: 'absolute', bottom: 10, padding: 10},
  textImageCard: {
    textAlign: 'justify',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 10,
    textShadowColor: '#000',
  },
  itemCardDetail: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'center',
  },
  itemTextCard: {
    color: 'white',
    fontSize: 14,
    color: '#fff',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 10,
    textShadowColor: '#000',
    margin: 5,
  },
  icon: {
    fontSize: 18,
    color: '#fff',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 10,
    textShadowColor: '#000',
  },
});

export default Search;
