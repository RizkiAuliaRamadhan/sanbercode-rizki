import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import Axios from 'axios';
import {Icon, Image} from 'react-native-elements';

const {width, height} = Dimensions.get('window');

const Detail = ({route, navigation}) => {
  const {key} = route.params;

  const [items, setItems] = useState({});
  const [author, setAuthor] = useState({});
  const [needItem, setNeedItem] = useState([]);
  const [bahan, setBahan] = useState([]);
  const [step, setStep] = useState([]);

  let i = 1;

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    Axios.get(`https://masak-apa.tomorisakura.vercel.app/api/recipe/${key}`)
      .then(res => {
        setItems(res.data.results);
        setAuthor(res.data.results.author);
        setNeedItem(res.data.results.needItem);
        setBahan(res.data.results.ingredient);
        setStep(res.data.results.step);
      })
      .catch(err => {
        console.log(err);
      });
  };
  // console.log(ingredients);
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.container}>
        <TouchableOpacity
          style={{flexDirection: 'row', alignItems: 'center'}}
          onPress={() => {
            navigation.goBack();
          }}>
          <Icon
            name="arrow-back-circle-sharp"
            color="#007bff"
            size={30}
            type="ionicon"
          />
          <View style={{width: 10}} />
        </TouchableOpacity>
        <View style={{marginTop: 5}}>
          <Text style={styles.title}>{items.title}</Text>
          <View style={styles.cardIcon}>
            <Icon
              name="alarm"
              color="#007bff"
              style={styles.icon}
              type="ionicon"
            />
            <Text style={styles.iconText}>{items.times}</Text>
            <View style={{width: 10}} />
            <Icon
              name="cafe"
              color="#007bff"
              style={styles.icon}
              type="ionicon"
            />
            <Text style={styles.iconText}>{items.servings}</Text>
            <View style={{width: 10}} />
            <Icon
              name="pulse"
              color="#007bff"
              style={styles.icon}
              type="ionicon"
            />
            <Text style={styles.iconText}>{items.dificulty}</Text>
          </View>
        </View>
        <View style={{marginBottom: 10}}>
          <Text style={styles.textAuthor}>
            {author.user} | {author.datePublished}
          </Text>
        </View>
        <View>
          <Image
            style={styles.image}
            source={{uri: items.thumb}}
            transition
            transitionDuration="2000"
          />
        </View>
        <View style={{marginTop: 10}}>
          <Text style={styles.textDesc}>{items.desc}</Text>
        </View>
        <View style={{marginTop: 10}}>
          <Text style={{textAlign: 'center', fontSize: 20, fontWeight: 'bold'}}>
            Bahan-bahan
          </Text>
          {bahan.map((value, index, array) => {
            return (
              <Text style={{fontSize: 16}}>
                {index + 1}. {array[index]}
              </Text>
            );
          })}
        </View>
        <View style={{marginTop: 10}}>
          <Text style={{textAlign: 'center', fontSize: 20, fontWeight: 'bold'}}>
            Cara membuat
          </Text>
          {step.map((value, index, array) => {
            return (
              <Text style={{fontSize: 16}}>
                {index + 1}. {array[index].substr(2)}
              </Text>
            );
          })}
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  cardIcon: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  icon: {
    fontSize: 20,
  },
  iconText: {
    color: '#007bff',
    fontSize: 16,
    shadowColor: '#000',
    shadowOffset: {width: 0.8, height: 0.8},
    shadowOpacity: 1,
    shadowRadius: 3,
    elevation: 5,
    margin: 5,
  },
  textAuthor: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold',
  },
  image: {
    width: '100%',
    height: height / 3,
    borderRadius: 5,
  },
  textDesc: {
    textAlign: 'justify',
    fontSize: 16,
  },
});

export default Detail;
