import React from 'react';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';

const Welcome = ({navigation}) => {
  return (
    <ImageBackground
      source={require('../../images/1.jpg')}
      style={{
        width: '100%',
        height: '100%',
        flex: 1,
        justifyContent: 'space-between',
      }}>
      <View>
        <Text style={styles.title}>Selamat Datang</Text>
      </View>
      <View style={styles.card}>
        <TouchableOpacity
          style={styles.masuk}
          onPress={() => navigation.navigate('Masuk')}>
          <Text style={styles.textTombol}>Masuk</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.daftar}
          onPress={() => navigation.navigate('Daftar')}>
          <Text style={styles.textTombol}>Daftar</Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  title: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 30,
    textAlign: 'center',
    marginTop: 100,
  },
  card: {
    height: '25%',
    backgroundColor: '#F9F9F9',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderWidth: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  masuk: {
    width: '75%',
    backgroundColor: '#007bff',
    padding: 10,
    borderRadius: 10,
  },
  daftar: {
    width: '75%',
    backgroundColor: '#28a745',
    padding: 10,
    marginTop: 10,
    borderRadius: 10,
  },
  textTombol: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 16,
  },
});

export default Welcome;
