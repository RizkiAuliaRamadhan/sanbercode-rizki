import React, {useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Alert,
} from 'react-native';
import auth from '@react-native-firebase/auth';
import {Input, Icon} from 'react-native-elements';

const Daftar = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [password2, setPassword2] = useState('');

  const [security, setSecurity] = useState(true);
  const [security2, setSecurity2] = useState(true);

  const onRegisterPress = () => {
    if (email == '' || password == '') {
      Alert.alert('Gagal', 'Form harus diisi');
    } else if (password !== password2) {
      Alert.alert('Gagal', 'Password harus sama');
    } else {
      auth()
        .createUserWithEmailAndPassword(email, password)
        .then(res => {
          Alert.alert('Berhasil', 'Selamat Anda berhasil daftar');
          navigation.navigate('Masuk');
        })
        .catch(err => {
          Alert.alert('Gagal', err.message);
        });
    }
  };
  return (
    <ImageBackground
      source={require('../../images/1.jpg')}
      style={{
        width: '100%',
        height: '100%',
        flex: 1,
        justifyContent: 'flex-end',
      }}>
      <View style={styles.card}>
        <Input
          placeholder="email@address.com"
          label="Email"
          value={email}
          onChangeText={email => setEmail(email)}
          leftIcon={<Icon name="mail" type="ionicon" color="#888" />}
        />
        <Input
          placeholder="Password"
          label="Password"
          value={password}
          onChangeText={password => setPassword(password)}
          leftIcon={<Icon name="lock-closed" type="ionicon" color="#888" />}
          rightIcon={
            <Icon
              name={security ? 'eye-off' : 'eye'}
              type="ionicon"
              color="#888"
              onPress={() => {
                setSecurity(prev => !prev);
              }}
            />
          }
          secureTextEntry={security}
        />
        <Input
          placeholder="Konfirmasi Password"
          label="Password"
          value={password2}
          onChangeText={password => setPassword2(password)}
          leftIcon={<Icon name="lock-closed" type="ionicon" color="#888" />}
          rightIcon={
            <Icon
              name={security2 ? 'eye-off' : 'eye'}
              type="ionicon"
              color="#888"
              onPress={() => {
                setSecurity2(prev => !prev);
              }}
            />
          }
          secureTextEntry={security2}
        />
        <TouchableOpacity
          style={styles.daftar}
          onPress={() => onRegisterPress()}>
          <Text style={styles.textTombol}>Daftar</Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  card: {
    height: '80%',
    backgroundColor: '#F9F9F9',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderWidth: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: '10%',
  },
  daftar: {
    width: '100%',
    backgroundColor: '#28a745',
    padding: 10,
    borderRadius: 10,
    marginTop: 20,
  },
  textTombol: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 16,
  },
  label: {
    width: '75%',
    opacity: 0.5,
    marginBottom: 5,
    marginTop: 5,
  },
  input: {
    borderWidth: 1,
    borderColor: '#17a2b8',
    width: '75%',
    paddingVertical: -10,
    paddingHorizontal: 10,
    borderRadius: 5,
  },
});

export default Daftar;
