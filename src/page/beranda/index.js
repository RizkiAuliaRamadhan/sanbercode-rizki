import React, {useState, useEffect} from 'react';
import {ScrollView, View, Text} from 'react-native';
import Axios from 'axios';
import {Icon} from 'react-native-elements';

import Carousel from '../../components/Carousel';
import Category from '../../components/Category';
import Resep from '../../components/Resep';

const Beranda = ({route, navigation}) => {
  const [items, setItems] = useState([]);
  const {email} = route.params;

  console.log(email);

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    Axios.get(
      'https://masak-apa.tomorisakura.vercel.app/api/recipes-length/?limit=4',
    )
      .then(res => {
        setItems(res.data.results);
      })
      .catch(err => console.log(err));
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View
        style={{
          marginTop: 5,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginHorizontal: 10,
        }}>
        <View style={{flexDirection: 'column'}}>
          <Text style={{fontWeight: 'bold'}}>Mau masak apa hari ini,</Text>
          <Text style={{fontWeight: 'bold'}}>{email} ?</Text>
        </View>
        <Icon
          name="search-circle-sharp"
          type="ionicon"
          color="#007bff"
          size={40}
          onPress={() => {
            navigation.navigate('Search');
          }}
        />
      </View>
      <Carousel data={items} />
      <Category />
      <Resep />
    </ScrollView>
  );
};

export default Beranda;
