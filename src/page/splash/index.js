import React, {useEffect} from 'react';
import {Text, View, ImageBackground, Image} from 'react-native';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Welcome');
    }, 2000);
  }, []);
  return (
    <ImageBackground
      source={require('../../images/1.jpg')}
      style={{
        width: '100%',
        height: '100%',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Image
        source={require('../../images/logo.png')}
        style={{width: 250, height: 250}}
      />
    </ImageBackground>
  );
};

export default Splash;
