import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Alert,
} from 'react-native';
import Axios from 'axios';
import {Icon, Image} from 'react-native-elements';

const CategoryResep = ({route, navigation}) => {
  const {key} = route.params;

  const [items, setItems] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    Axios.get(
      `https://masak-apa.tomorisakura.vercel.app/api/categorys/recipes/${key}`,
    )
      .then(res => {
        setItems(res.data.results);
      })
      .catch(err => {
        Alert.alert('Gagal', 'Maaf data gagal ditampilkan');
        navigation.goBack();
      });
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.container}>
        <TouchableOpacity
          style={{flexDirection: 'row', alignItems: 'center'}}
          onPress={() => {
            navigation.goBack();
          }}>
          <Icon
            name="arrow-back-circle-sharp"
            color="#007bff"
            size={30}
            type="ionicon"
          />
          <View style={{width: 10}} />
        </TouchableOpacity>
      </View>
      <View style={{paddingHorizontal: 10}}>
        <Text style={styles.title}>{key}</Text>
        {items.map((value, index, array) => {
          return (
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('Detail', {
                  key: array[index].key,
                });
              }}>
              <Image
                source={{uri: array[index].thumb}}
                style={styles.image}
                resizeMode="cover"
                transition
                transitionDuration="1000"
              />
              <View style={styles.card}>
                <Text style={styles.textImageCard}>{array[index].title}</Text>
                <View style={styles.itemCardDetail}>
                  <Icon
                    name="alarm"
                    color="#fff"
                    type="ionicon"
                    style={styles.icon}
                  />
                  <Text style={styles.itemTextCard}>{array[index].times}</Text>
                  <View style={{width: 10}} />
                  <Icon
                    name="cafe"
                    color="#fff"
                    type="ionicon"
                    style={styles.icon}
                  />
                  <Text style={styles.itemTextCard}>
                    {array[index].portion}
                  </Text>
                  <View style={{width: 10}} />
                  <Icon
                    name="pulse"
                    color="#fff"
                    type="ionicon"
                    style={styles.icon}
                  />
                  <Text style={styles.itemTextCard}>
                    {array[index].dificulty}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          );
        })}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  image: {
    width: '100%',
    height: 200,
    marginVertical: 5,
    borderRadius: 5,
  },
  card: {position: 'absolute', bottom: 10, padding: 10},
  textImageCard: {
    textAlign: 'justify',
    color: '#FFF',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 10,
    textShadowColor: '#000',
  },
  itemCardDetail: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'center',
  },
  itemTextCard: {
    color: 'white',
    fontSize: 14,
    color: '#fff',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 10,
    textShadowColor: '#000',
    margin: 5,
  },
  icon: {
    fontSize: 18,
    color: '#fff',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 10,
    textShadowColor: '#000',
  },
});

export default CategoryResep;
