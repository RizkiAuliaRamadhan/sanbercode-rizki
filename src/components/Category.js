import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Axios from 'axios';
import {useNavigation} from '@react-navigation/core';

const Category = () => {
  const [items, setItems] = useState([]);
  const navigation = useNavigation();

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    Axios.get('https://masak-apa.tomorisakura.vercel.app/api/categorys/recipes')
      .then(res => {
        setItems(res.data.results);
      })
      .catch(err => {
        console.log(err);
      });
  };
  return (
    <View style={{paddingHorizontal: 10}}>
      <Text style={styles.title}>Cari bedasarkan kategori</Text>
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        {items.map((value, index, array) => {
          return (
            <TouchableOpacity
              style={styles.kategori}
              onPress={() => {
                navigation.navigate('CategoryResep', {
                  key: array[index].key,
                });
              }}>
              <Text style={{color: '#fff'}}>{array[index].category}</Text>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginVertical: 10,
  },
  kategori: {
    padding: 5,
    marginVertical: 5,
    marginHorizontal: 3,
    backgroundColor: '#007bff',
    borderRadius: 5,
  },
});

export default Category;
