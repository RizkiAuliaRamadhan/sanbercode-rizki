import {useNavigation} from '@react-navigation/native';
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  FlatList,
  Animated,
  TouchableOpacity,
} from 'react-native';
import {Icon, Image} from 'react-native-elements';

const {width, height} = Dimensions.get('window');

const Carousel = ({data}) => {
  const scrollX = new Animated.Value(0);
  let position = Animated.divide(scrollX, width);
  const [dataList, setDataList] = useState(data);
  const navigation = useNavigation();

  useEffect(() => {
    setDataList(data);
  });

  if (data && data.length) {
    return (
      <View>
        <FlatList
          data={data}
          keyExtractor={(item, index) => 'key' + index}
          horizontal
          pagingEnabled
          scrollEnabled
          snapToAlignment="center"
          scrollEventThrottle={16}
          decelerationRate={'fast'}
          showsHorizontalScrollIndicator={false}
          renderItem={({item}) => {
            return (
              <TouchableOpacity
                style={styles.cardView}
                onPress={() => {
                  navigation.navigate('Detail', {
                    key: item.key,
                  });
                }}>
                <Image
                  style={styles.image}
                  source={{uri: item.thumb}}
                  transition
                />
                <View style={styles.textView}>
                  <Text style={styles.itemTitle}> {item.title}</Text>
                  <View style={styles.itemCardDetail}>
                    <Icon
                      name="alarm"
                      color="#fff"
                      style={styles.icon}
                      type="ionicon"
                    />
                    <Text style={styles.itemTimes}>{item.times}</Text>
                    <View style={{width: 10}} />
                    <Icon
                      name="cafe"
                      color="#fff"
                      style={styles.icon}
                      type="ionicon"
                    />
                    <Text style={styles.itemTimes}>{item.portion}</Text>
                    <View style={{width: 10}} />
                    <Icon
                      name="pulse"
                      color="#fff"
                      style={styles.icon}
                      type="ionicon"
                    />
                    <Text style={styles.itemTimes}>{item.dificulty}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            );
          }}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {x: scrollX}}}],
            {useNativeDriver: false},
          )}
        />

        <View style={styles.dotView}>
          {data.map((_, i) => {
            let opacity = position.interpolate({
              inputRange: [i - 1, i, i + 1],
              outputRange: [0.3, 1, 0.3],
              extrapolate: 'clamp',
            });
            return (
              <Animated.View
                key={i}
                style={{
                  opacity,
                  height: 10,
                  width: 10,
                  backgroundColor: '#595959',
                  margin: 5,
                  borderRadius: 5,
                }}
              />
            );
          })}
        </View>
      </View>
    );
  }

  console.log('Please provide Images');
  return null;
};

const styles = StyleSheet.create({
  dotView: {flexDirection: 'row', justifyContent: 'center'},
  cardView: {
    flex: 1,
    width: width - 20,
    height: height / 3,
    backgroundColor: 'white',
    marginHorizontal: 10,
    marginTop: 5,
    marginBottom: 10,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0.5, height: 0.5},
    shadowOpacity: 0.5,
    shadowRadius: 3,
    elevation: 5,
  },

  textView: {
    position: 'absolute',
    bottom: 10,
    margin: 10,
    left: 5,
  },
  image: {
    width: width - 20,
    height: height / 3,
    borderRadius: 10,
  },
  itemTitle: {
    color: 'white',
    textAlign: 'center',
    fontSize: 22,
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 10,
    textShadowColor: '#000',
    marginBottom: 5,
    fontWeight: 'bold',
    elevation: 5,
  },
  itemCardDetail: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemTimes: {
    color: 'white',
    fontSize: 14,
    color: '#fff',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 10,
    textShadowColor: '#000',
    elevation: 5,
    margin: 5,
  },
  icon: {
    fontSize: 18,
    color: '#fff',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 10,
    textShadowColor: '#000',
  },
});

export default Carousel;
