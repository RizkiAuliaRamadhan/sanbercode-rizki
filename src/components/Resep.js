import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Axios from 'axios';
import {Icon, Image} from 'react-native-elements';
import {useNavigation} from '@react-navigation/core';

const Resep = () => {
  const [items, setItems] = useState([]);

  const navigation = useNavigation();

  useEffect(() => {
    getData();
  }, []);
  const getData = () => {
    Axios.get('https://masak-apa.tomorisakura.vercel.app/api/recipes/3')
      .then(res => {
        setItems(res.data.results);
      })
      .catch(err => console.log(err));
  };

  return (
    <View style={{marginTop: 20, paddingHorizontal: 10}}>
      <Text style={styles.title}>Resep Terbaru</Text>
      {items.map((value, index, array) => {
        return (
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Detail', {
                key: array[index].key,
              });
            }}>
            <Image
              source={{uri: array[index].thumb}}
              style={styles.image}
              resizeMode="cover"
              transition
              transitionDuration="1000"
            />
            <View style={styles.card}>
              <Text style={styles.textImageCard}>{array[index].title}</Text>
              <View style={styles.itemCardDetail}>
                <Icon
                  name="alarm"
                  color="#fff"
                  style={styles.icon}
                  type="ionicon"
                />
                <Text style={styles.itemTextCard}>{array[index].times}</Text>
                <View style={{width: 10}} />
                <Icon
                  name="cafe"
                  color="#fff"
                  style={styles.icon}
                  type="ionicon"
                />
                <Text style={styles.itemTextCard}>{array[index].portion}</Text>
                <View style={{width: 10}} />
                <Icon
                  name="pulse"
                  color="#fff"
                  style={styles.icon}
                  type="ionicon"
                />
                <Text style={styles.itemTextCard}>
                  {array[index].dificulty}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  image: {
    width: '100%',
    height: 200,
    marginVertical: 5,
    borderRadius: 5,
  },
  card: {position: 'absolute', bottom: 10, padding: 10},
  textImageCard: {
    textAlign: 'justify',
    color: '#FFF',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 10,
    textShadowColor: '#000',
  },
  itemCardDetail: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'center',
  },
  itemTextCard: {
    color: 'white',
    fontSize: 14,
    color: '#fff',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 10,
    textShadowColor: '#000',
    margin: 5,
  },
  icon: {
    fontSize: 18,
    color: '#fff',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 10,
    textShadowColor: '#000',
  },
});

export default Resep;
