import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Icon} from 'react-native-elements';

import Splash from '../page/splash';
import Welcome from '../page/welcome';
import Masuk from '../page/masuk';
import Daftar from '../page/daftar';
import Beranda from '../page/beranda';
import Tentang from '../page/tentang';
import Detail from '../page/Detail';
import CategoryResep from '../components/CategoryResep';
import Search from '../page/search';

const Tab = createBottomTabNavigator();
const Home = createStackNavigator();
const screenOptionStyle = {
  headerShown: false,
};

const HomeStack = ({route}) => {
  const {email} = route.params;
  // console.log(email);
  return (
    <Home.Navigator screenOptions={screenOptionStyle}>
      <Home.Screen
        name="Beranda"
        component={Beranda}
        initialParams={{email: email}}
      />
      <Home.Screen name="Detail" component={Detail} />
      <Home.Screen name="CategoryResep" component={CategoryResep} />
      <Home.Screen name="Search" component={Search} />
    </Home.Navigator>
  );
};

const BottomNavigator = ({route}) => {
  const {email} = route.params;
  return (
    <Tab.Navigator
      tabBarOptions={{
        style: {
          height: 50,
          justifyContent: 'center',
          paddingVertical: 10,
          elevation: 2,
        },
        activeTintColor: '#007bff',
        inactiveTintColor: 'gray',
      }}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;

          if (route.name === 'HomeStack') {
            iconName = 'home';
          } else if (route.name === 'Tentang') {
            iconName = 'person';
          }
          return <Icon name={iconName} color={color} type="ionicon" />;
        },
        tabBarLabel: '',
      })}>
      <Tab.Screen
        name="HomeStack"
        component={HomeStack}
        initialParams={{email: email}}
      />
      <Tab.Screen name="Tentang" component={Tentang} />
    </Tab.Navigator>
  );
};

const Stack = createStackNavigator();

const Route = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={screenOptionStyle}>
        <Stack.Screen name="Splash" component={Splash} />
        <Stack.Screen name="Welcome" component={Welcome} />
        <Stack.Screen name="Masuk" component={Masuk} />
        <Stack.Screen name="Daftar" component={Daftar} />
        <Stack.Screen name="Bottom" component={BottomNavigator} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Route;
